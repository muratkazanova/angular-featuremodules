import { Component, OnInit } from '@angular/core';
import { IEmployee } from 'src/app/models/employee';
import { EmployeeService } from '../../Services/employee.service';
@Component({
  selector: 'app-employee-dashboard',
  templateUrl: './employee-dashboard.component.html',
  styleUrls: ['./employee-dashboard.component.scss']
})
export class EmployeeDashboardComponent implements OnInit {
  employees = [];
  isNewEmployee: boolean;
  constructor(private employeeService: EmployeeService) {}

  ngOnInit() {
    this.employeeService.EmployeeFormEvent.subscribe(s => {
      this.isNewEmployee = s;
    });
  }
}
