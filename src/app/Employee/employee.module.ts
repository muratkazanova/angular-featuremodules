import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  EmployeeDashboardComponent,
  EmployeeTaskbarComponent,
  EmployeeFormComponent,
  EmployeeItemComponent,
  EmployeeListComponent
} from './index';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    EmployeeDashboardComponent,
    EmployeeTaskbarComponent,
    EmployeeListComponent,
    EmployeeItemComponent,
    EmployeeFormComponent
  ],
  imports: [CommonModule, SharedModule],
  exports: [EmployeeDashboardComponent]
})
export class EmployeeModule {}
