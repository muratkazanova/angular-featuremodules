import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IEmployee } from 'src/app/models/employee';
import { EmployeeService } from '../../Services/employee.service';
import { switchMap } from 'rxjs/operators';
@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit {
  employees: IEmployee[];
  @Output()
  EmployeeDelete: EventEmitter<number> = new EventEmitter();
  @Output()
  EmployeeUpdate: EventEmitter<IEmployee> = new EventEmitter();
  constructor(private employeeService: EmployeeService) {}

  ngOnInit() {
    this.employeeService.EmployeeSourceChanges.pipe(
      switchMap(_ => this.employeeService.getEmployees())
    ).subscribe((data: IEmployee[]) => {
      this.employees = data;
    });
  }
}
